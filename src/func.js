const getSum = (str1, str2) => {
  const strLength1 = str1.length
  const strLength2 = str2.length
  const maxLength = Math.max(strLength1, strLength2)

  let carry = 0, sum = ''

  for (let i = 1; i <= maxLength; i++) {
    let a = +str1.charAt(strLength1 - i);
    let b = +str2.charAt(strLength2 - i);

    let t = carry + a + b
    carry = t/10 |0
    t %= 10

    sum = (i === maxLength && carry) ? carry*10 + t + sum : t + sum
      
  }

  return sum
};


const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCounter = 0;
  let commentCounter = 0;
  listOfPosts.forEach(element => {
    
    if(element.author === authorName) {
      postCounter +=1
    } 
    if(Array.isArray(element.comments)){
    element.comments.forEach(item => {
        if(item.author === authorName) {
          commentCounter +=1;
        } else {
          return 'Post:0,comments:0';
        }
      });
    }
  });
  return `Post:${postCounter},comments:${commentCounter}`
};





const tickets=(people)=> {
  let q25 = 0;
  let q50 = 0;
  let q100 = 0;
  for (let i = 0; i < people.length; i++){
    if(+people[i] == 25) {
      q25 += +people[i]
    } else if(+people[i] == 50) {
      q25 -= 25;
      q50 += 50
      if( q25 < 0) {
         return 'NO'
      } 
    } else if(+people[i] == 100) {
      q100 += +people[i];
      if(q50>=50) {
        q50 -=50
        q25 -=25
      } else {
        q25 -=75
      }
       if( q25 < 0) {
        return 'NO'
      }
    }
     console.log(q25, q50, q100)
  } return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
